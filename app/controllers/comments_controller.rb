class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: [:destroy]

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      flash[:success] = "Comment created!"
      redirect_to request.referrer || root_path
    else
     flash[:danger] = "Comment are empty or too long"
     redirect_to request.referrer || root_path
    end
  end
  
  def destroy
    @comment.destroy
    flash[:success] = "Comment deleted"
    redirect_to request.referrer || root_path
  end
  
    private
    
    def comment_params
      params.require(:comment).permit(:content,:user_id,:micropost_id)
    end
    
    def correct_user
      @comment = current_user.comments.find_by(id: params[:id])
      redirect_to root_path if @comment.nil?
    end
end
