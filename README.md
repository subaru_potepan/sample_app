# README

## 名前


sample app


heroku 
https://tranquil-forest-35860.herokuapp.com/


login email address : example@railstutorial.org
password : foobar

## 概要


Rails Tutorialで作成したアプリにコメント機能とユーザー検索機能を加えたものです。
progateで基本を勉強してから、Railsの勉強を行う上で初めて作った本格的なアプリになります。

## 苦労した点


最初は全体的に何がなんだかわからず、かなり苦労しました。
特にモデルの関連付けがなかなか理解できず、どうしたものか。。と考え悩みました。  それでも勉強を進めて行くうちにだんだんと理解できるようになってきました。


他にもerrorの対応等も初めて行ったので、errorが解決できず、どうすればいいんだ。。という状況に陥ったり、  webの知識も全くなかったので言っている意味がわからなかったりと様々な事につまずきました。


## 学んだ事


ザックリとしたアプリケーションの作り方の基礎や、問題が起きた時には問題点を推測して、解決法を調べ一つ一つ試し解決に導いて行くという事を学びました。



[*Ruby on Rails チュートリアル*](https://railstutorial.jp/)
[Michael Hartl](http://www.michaelhartl.com/) 著

